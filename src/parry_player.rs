use std::f32::consts::PI;

use parry2d::{na::{Point2, Vector2}, shape::Polyline, math::{Isometry, Rotation}};
use sdl2::{render::{Texture, Canvas}, rect::{Rect, Point}, video::Window, pixels::Color, keyboard::{KeyboardState, Scancode}};

#[derive(Clone)]
pub struct Sprite<'a>{
    texture:&'a Texture<'a>,
    src_rect:Rect,
    transform:Isometry<f32>,
    poly_boundry:Polyline,
    z:i32,
    delete:bool
}

pub struct Player<'a>{
    sprite:Sprite<'a>
}

pub struct Meteor<'a>{
    sprite:Sprite<'a>
}

pub struct Lazer<'a>{
    sprite:Sprite<'a>
}

#[derive(PartialEq,Debug)]
pub enum return_action{
    spawn_lazer(Isometry<f32>),
    nothing,
}

fn draw(sprite:&Sprite,canvas:&mut Canvas<Window>){
    // let (canvas_width, canvas_height) = canvas.output_size().expect("The size of canvas is retrievable");
    let dst_rect = Rect::new(
        // (sprite.dst.x * canvas_width as f32) as i32 - (sprite.src_rect.width() as f32 / 2.0) as i32,
        // (sprite.dst.y * canvas_height as f32) as i32 - (sprite.src_rect.height() as f32 / 2.0) as i32,
        sprite.transform.translation.x as i32 - (sprite.src_rect.width() as f32 / 2.0) as i32,
        sprite.transform.translation.y as i32 - (sprite.src_rect.height() as f32 / 2.0) as i32,
        sprite.src_rect.width(),
        sprite.src_rect.height()
    );

    let mut rotation_as_degree = sprite.transform.rotation.angle() / (2.0 * PI) * 360.0;

    if rotation_as_degree < 0.0 {
        rotation_as_degree += 360.0;
    }

    canvas.copy_ex(
        sprite.texture,
        sprite.src_rect,
        dst_rect,
        rotation_as_degree.into(),
        None,
        false,
        false
    ).unwrap();
}

fn draw_bounding_box(sprite:&Sprite,canvas:&mut Canvas<Window>){

    let mut points = Vec::new();
    
    for i in sprite.poly_boundry.vertices(){
        let temp = *i;
        points.push(Point2::from(temp));
    }

    parry2d::transformation::utils::transform(&mut points[..], sprite.transform);

    let mut sdl_vec = Vec::new();
    let mut parry_point = Vec::new();
    for i in points{
        sdl_vec.push(Point::new(i.x as i32,i.y as i32));
        parry_point.push(Point2::new(i.x as f32, i.y as f32));
    }

    sdl_vec.push(sdl_vec[0]);

    let color = canvas.draw_color();
    canvas.set_draw_color(Color::RGB(255, 0, 0));
    canvas.draw_lines(&sdl_vec[..]).unwrap();
    canvas.set_draw_color(color);
}

pub fn check_collsion(sprite:&Sprite,sprite2:&Sprite) -> bool{

    parry2d::query::intersection_test(&sprite.transform, &sprite.poly_boundry, &sprite2.transform, &sprite2.poly_boundry).unwrap()
}

pub fn gen_new_sprite<'a>(texture:&'a Texture<'a>,src_rect:Rect,dst:Vector2<f32>,rotation:f32,collision_boundary:Option<Vec<Point2<f32>>>,z:i32) -> Sprite<'a>{
    let mut boundary = Vec::new();
    if let None = collision_boundary{
        boundary.push(Point2::new(src_rect.width() as f32 / -2.0, src_rect.height() as f32 /2.0));
        boundary.push(Point2::new(src_rect.width() as f32 / 2.0, src_rect.height() as f32 / 2.0));
        boundary.push(Point2::new(src_rect.width() as f32 / 2.0, src_rect.height() as f32 / -2.0));
        boundary.push(Point2::new(src_rect.width() as f32 / -2.0, src_rect.height() as f32 / -2.0));
    }else{
        for i in &collision_boundary.clone().unwrap(){
            boundary.push(*i);
        }
    }
    let new_transform = Isometry::new(dst,rotation);
    Sprite { 
        texture: texture,
        src_rect: src_rect,
        transform: new_transform,
        poly_boundry: Polyline::new(boundary,None),
        z:z,
        delete:false
    }
}

fn move_forward(sprite:&mut Sprite,dist:f32) {
    let angle = sprite.transform.rotation.angle();
    let x_move = dist * angle.sin();
    let y_move = dist * angle.cos();
    sprite.transform.translation.vector += Vector2::new(-x_move, y_move);
}

fn rotate(sprite:&mut Sprite,rot:f32){
    let curr_rot = sprite.transform.rotation.angle();
    sprite.transform.rotation = Rotation::from_angle(curr_rot+rot);
}

pub trait SpriteCreation<'a>{
    fn new(texture:&'a Texture,src_rect:Rect,dst:Vector2<f32>,rotation:f32,collision_boundary:Option<Vec<Point2<f32>>>,z:i32) -> Self;
}

pub trait SpriteGeneric<'a> {

    fn draw(&self,canvas:&mut Canvas<Window>){
        draw(&self.get_sprite(), canvas);
    }

    fn draw_collision(&self,canvas:&mut Canvas<Window>){
        draw_bounding_box(&self.get_sprite(),canvas);
    }

    fn update(&mut self);

    fn get_keyboard(&mut self,ks:KeyboardState) -> return_action;

    fn rotate(&mut self,rot:f32){
        rotate(&mut self.get_sprite_mut(), rot);
    }

    fn move_forward(&mut self,dist: f32){
        move_forward(&mut self.get_sprite_mut(), dist);
    }

    fn check_collsion(&self,sprite_2:&dyn SpriteGeneric) -> bool{
        check_collsion(&self.get_sprite(),sprite_2.get_sprite())
    }

    fn get_sprite(&self) -> &Sprite<'a>;

    fn get_sprite_mut(&mut self) -> &mut Sprite<'a>;

    fn get_z(&self) -> i32 {
        self.get_sprite().z
    }

    fn deleteable(&self) -> bool {
        self.get_sprite().delete
    }
}

impl<'a> SpriteCreation<'a> for Player<'a>{
    fn new(texture:&'a Texture,src_rect:Rect,dst:Vector2<f32>,rotation:f32,collision_boundary:Option<Vec<Point2<f32>>>,z:i32) -> Self {
        Player{sprite:gen_new_sprite(texture, src_rect, dst, rotation, collision_boundary,z)}
    }
}

impl<'a> SpriteGeneric<'a> for Player<'a> {
    
    fn update(&mut self) {
        // rotate(&mut self.sprite, 0.01);
        // self.move_forward(-1.0);
        // todo!("implement update for sprite");
    }

    fn get_keyboard(&mut self,ks:KeyboardState) -> return_action {
        let mut return_action = return_action::nothing;
        for i in ks.pressed_scancodes(){
            match i {
                Scancode::Left => self.rotate(PI/-100.0),
                Scancode::Right => self.rotate(PI/100.0),
                Scancode::Up => self.move_forward(-1.0),
                Scancode::Space => {
                    return_action = return_action::spawn_lazer(self.sprite.transform)
                },
                _ => ()
            }
        }
        return_action
    }

    fn get_sprite(&self) -> &Sprite<'a> {
        &self.sprite
    }

    fn get_sprite_mut(&mut self) -> &mut Sprite<'a> {
        &mut self.sprite
    }

}

impl<'a> SpriteCreation<'a> for Meteor<'a>{
    fn new(texture:&'a Texture,src_rect:Rect,dst:Vector2<f32>,rotation:f32,collision_boundary:Option<Vec<Point2<f32>>>,z:i32) -> Self {
        Meteor {sprite:gen_new_sprite(texture, src_rect, dst, rotation, collision_boundary,z)}
    }
}

impl<'a> SpriteGeneric<'a> for Meteor<'a> {

    fn update(&mut self) {

    }

    fn get_keyboard(&mut self,ks:KeyboardState) -> return_action{
        return_action::nothing
    }

    fn get_sprite(&self) -> &Sprite<'a> {
        &self.sprite
    }

    fn get_sprite_mut(&mut self) -> &mut Sprite<'a> {
        &mut self.sprite
    }

}


impl<'a> SpriteCreation<'a> for Lazer<'a>{
    fn new(texture:&'a Texture,src_rect:Rect,dst:Vector2<f32>,rotation:f32,collision_boundary:Option<Vec<Point2<f32>>>,z:i32) -> Self {
        Lazer {sprite:gen_new_sprite(texture, src_rect, dst, rotation, collision_boundary,z)}
    }
}

impl<'a> SpriteGeneric<'a> for Lazer<'a> {

    fn update(&mut self) {
        self.move_forward(-10.0);
        let x_pos = self.sprite.transform.translation.x;
        let y_pos = self.sprite.transform.translation.y;
        if y_pos < 0.0 || y_pos > 500.0{
            self.sprite.delete = true;
        }

        if x_pos < 0.0 || x_pos > 500.0{
            self.sprite.delete = true;
        }
    }

    fn get_keyboard(&mut self,ks:KeyboardState) -> return_action{
        return_action::nothing
    }
    
    fn get_sprite(&self) -> &Sprite<'a> {
        &self.sprite
    }

    fn get_sprite_mut(&mut self) -> &mut Sprite<'a> {
        &mut self.sprite
    }

}
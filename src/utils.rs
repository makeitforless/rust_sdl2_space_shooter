use xml::{reader::ParserConfig};
use std::fs;

pub fn read_kenney_xml(file_path:&String, image_name:&str) -> (i32,i32,u32,u32){
    let contents = fs::File::open(&file_path).expect("This file can be opened");
    let reader = ParserConfig::new().create_reader(contents);
    for i in reader{
        match i.unwrap() {
            xml::reader::XmlEvent::StartElement { name:_, attributes, namespace:_ } => 
            if attributes[0].value == image_name{
                return (
                    attributes[1].value.parse().expect("Value to be a number"),
                    attributes[2].value.parse().expect("Value to be a number"),
                    attributes[3].value.parse().expect("Value to be a number"),
                    attributes[4].value.parse().expect("Value to be a number")
                )
            },
            _ => ()
        }
    }
    panic!("could not find {} in file {}",image_name,file_path);
    // let wanted_xml = reader.into_iter().find(|x|
    //     match x.as_ref().unwrap() {
    //         xml::reader::XmlEvent::StartElement { name:_, attributes, namespace:_ } => {
    //             attributes[0].value == image_name
    //         },
    //         _ => false
    //     }
    // );
    // if wanted_xml == None{
        
    // }
    // match wanted_xml.unwrap().unwrap() {
    //     xml::reader::XmlEvent::StartElement { name:_, attributes, namespace:_ } => 
    //     (attributes[1].value.clone(),attributes[2].value.clone(),attributes[3].value.clone(),attributes[4].value.clone()),
    //     _ => panic!("unreachable")
    // }
    
}

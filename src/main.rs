// extern crate sdl2;

// mod utils;
// use crate::my_window::sprite_generic;
// mod parry_player;
// use parry2d;
// use parry2d::math::Point as Point2;
// use sdl2::rect::Rect;
// use sdl2::surface::Surface;
// use sdl2::sys::KeyCode;
// use sdl2::video::DisplayMode;
// use utils::read_kenney_xml;

// mod my_window;
// use my_window::{player, asteroid, lazer_ball, star};

// use sdl2::pixels::Color;
// use sdl2::event::Event;
// use sdl2::keyboard::Keycode;
// use std::path::Path;
// use std::time::Duration;

// pub fn main() {
//     //needed to use sdl2, creates the sdl2 context
//     let sdl_context = sdl2::init().unwrap();
//     let video_subsystem = sdl_context.video().unwrap();

//     //create the window that will be drawn in
//     let mut window = video_subsystem.window("rust-sdl2 demo", 800, 800)
//         .position_centered()
//         .build()
//         .unwrap();

//     //create a drawable canvas from the window
//     let mut canvas = window.into_canvas().build().unwrap();

//     //create a surface from the packed png
//     let temp_surface:Surface = sdl2::image::LoadSurface::from_file(Path::new("assets/kenney_space/sheet.png")).unwrap();
    
//     //create a texture creator (hold all textures so need to be save with let)
//     let texture_creator = canvas.texture_creator();

//     //create a texture from the loaded surface
//     let texture = texture_creator
//         .create_texture_from_surface(&temp_surface)
//         .map_err(|e| e.to_string()).unwrap();

//     //get the src rect that descibes where a given image is on the larger texture
//     let mut source_rect_0 = Rect::from(read_kenney_xml(&"assets/kenney_space/sheet.xml".to_string(), "playerShip3_green.png"));

//     //get the src rect that descibes where a given image is on the larger texture
//     let mut source_rect_1 = Rect::from(read_kenney_xml(&"assets/kenney_space/sheet.xml".to_string(), "meteorGrey_big4.png"));

//     let mut source_rect_2 = Rect::from(read_kenney_xml(&"assets/kenney_space/sheet.xml".to_string(), "laserBlue08.png"));

//     let mut source_rect_3 = Rect::from(read_kenney_xml(&"assets/kenney_space/sheet.xml".to_string(), "laserGreen14.png"));


//     //define a point to draw at (the point will be the midpoint of the image and is from 0..=1)
//     let mut mid_point = Point2::<f32>::new(0.5,0.5);

//     //create a sprite from the texture at the source rect with the given location
//     let mut newSprite = player::new(&texture,source_rect_0,mid_point,45.0,None);

//     //create a sprite from the texture at the source rect with the given location
//     let asteroid_sprite = asteroid::new(&texture,source_rect_1,mid_point,-45.0,None);

//     let lazer_ball = lazer_ball::new(&texture,source_rect_2,mid_point,0.0,None);

//     //create a vec of sprites to draw later
//     let mut sprite_vec:Vec<Box<dyn sprite_generic>> = Vec::new();
//     //add the created sprites
//     sprite_vec.push(Box::new(newSprite));
//     // sprite_vec.push(Box::new(asteroid_sprite));
//     sprite_vec.push(Box::new(lazer_ball));

//     //set background color and draw
//     canvas.set_draw_color(Color::RGB(0, 255, 255));
//     canvas.clear();
//     canvas.present();

//     let font_handler = sdl2::ttf::init().unwrap();
//     let font = font_handler.load_font("assets/kenney_space/kenvector_future.ttf", 25).unwrap();

//     let (window_width,window_height) = canvas.output_size().unwrap();

//     //get an event pump to read window events(mouse, key, resize, etc)
//     let mut event_pump = sdl_context.event_pump().unwrap();
//     let mut font_fps:String = "Wait".to_string();
//     let timer = sdl_context.timer().unwrap();
//     let mut start_ticks = timer.ticks();
//     //main loop
//     'running: loop {
        
//         canvas.clear();
//         for key in event_pump.keyboard_state().pressed_scancodes().into_iter(){
//             match key{
//                 sdl2::keyboard::Scancode::Left => sprite_vec[0].rotate(-1.0),
//                 sdl2::keyboard::Scancode::Right => sprite_vec[0].rotate(1.0),
//                 sdl2::keyboard::Scancode::Up => sprite_vec[0].move_forward(0.005),
//                 sdl2::keyboard::Scancode::Down => sprite_vec[0].move_forward(-0.005),
//                 _ => ()
//             }
//         }

//         if event_pump.mouse_state().left(){
//             for i in 0..=5000{
//                 let mouse_point = Point2::<f32>::new(event_pump.mouse_state().x() as f32 / window_width as f32,event_pump.mouse_state().y() as f32 / window_height as f32);
//                 let new_start = star::new(&texture,source_rect_3,mouse_point,0.0,None);
//                 sprite_vec.push(Box::new(new_start));
//             }
//         }
//         for event in event_pump.poll_iter() {
//             match event {
//                 Event::Quit {..} |
//                 Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
//                     break 'running
//                 },
//                 _ => ()
//         //         Event::KeyDown {  keycode:Some(Keycode::Left), .. } => {
//         //             sprite_vec[0].rotate(-1.0);
//         //         },
//         //         Event::KeyDown {  keycode:Some(Keycode::Right), .. } => {
//         //             sprite_vec[0].rotate(1.0);
//         //         },
//         //         Event::KeyDown {  keycode:Some(Keycode::Up), .. } => {
//         //             sprite_vec[0].move_forward(0.01);
//         //         },
//         //         Event::KeyDown {  keycode:Some(Keycode::Down), .. } => {
//         //             sprite_vec[0].move_forward(-0.01);
//         //         },
//         //         _ => {}
//             }
//         }
//         //loops through sprites and draws each one
//         for i in sprite_vec.iter_mut(){
//             // i.update();
//             i.draw(&mut canvas);
//             i.draw_bounding_box(&mut canvas);
//         }
//         sprite_vec.retain(|x| !x.deleteable());
//         let curr_ticks = timer.ticks();
//         let mut elsapsed_time_in_ns = (curr_ticks - start_ticks)* 1_000_000;
//         start_ticks = curr_ticks;
//         let frame_delay = 1_000_000_000u32 / 60;
//         if curr_ticks %1000 < 50{
//             font_fps = format!("FPS:{}",if elsapsed_time_in_ns < frame_delay {60} else {6_000_000_00 / (elsapsed_time_in_ns)});
//         }
//         if elsapsed_time_in_ns > frame_delay {
//             elsapsed_time_in_ns = frame_delay;
//         }
        
//         let font_text = font.render(&font_fps).solid(Color::RGB(255, 0, 0)).unwrap();
//         let font_tex = texture_creator.create_texture_from_surface(font_text).unwrap();
//         let (text_width,text_height) = font.size_of(&font_fps).unwrap();
//         let text_rect = Rect::new(100, 100, text_width,text_height);
//         canvas.copy_ex(&font_tex,None,text_rect,0.0,None,false,false).unwrap();
//         canvas.present();
        
        
//         ::std::thread::sleep(Duration::new(0, frame_delay - elsapsed_time_in_ns));
//     }
// }

//======================================================V2=========================================================

mod parry_player;
use std::f32::consts::PI;
use std::path::Path;
use std::time::{Duration,SystemTime};

use parry2d::na::{Point2, Vector2};
use parry2d::math::Isometry;
use parry_player::{gen_new_sprite, SpriteCreation, Player, Meteor, SpriteGeneric, Lazer, return_action};

mod utils;
use sdl2::rect::{Rect};
use sdl2::surface::Surface;
use utils::read_kenney_xml;

use sdl2::keyboard::Keycode;
use sdl2::event::Event;



fn main(){
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    //create the window that will be drawn in
    let window = video_subsystem.window("rust-sdl2 demo", 800, 800)
        .position_centered()
        .build()
        .unwrap();

    //create a drawable canvas from the window
    let mut canvas = window.into_canvas().build().unwrap();

    //create a surface from the packed png
    let temp_surface:Surface = sdl2::image::LoadSurface::from_file(Path::new("assets/kenney_space/sheet.png")).unwrap();
    
    //create a texture creator (hold all textures so need to be save with let)
    let texture_creator = canvas.texture_creator();

    //create a texture from the loaded surface
    let texture = texture_creator
        .create_texture_from_surface(&temp_surface)
        .map_err(|e| e.to_string()).unwrap();

    //get the src rect that descibes where a given image is on the larger texture
    let player_ship_rect = Rect::from(read_kenney_xml(&"assets/kenney_space/sheet.xml".to_string(), "playerShip3_green.png"));

    let asteroid_rect = Rect::from(read_kenney_xml(&"assets/kenney_space/sheet.xml".to_string(), "meteorBrown_big1.png"));

    let lazer_rect = Rect::from(read_kenney_xml(&"assets/kenney_space/sheet.xml".to_string(), "laserGreen14.png"));

    let ship_width = player_ship_rect.width() as f32 + 25.0;
    let ship_height = player_ship_rect.height() as f32;

    let mut player_collision = vec![
        Point2::new(ship_width / 2.0, 0.0),
        Point2::new(0.0,ship_height),
        Point2::new(ship_width,ship_height),
        Point2::new(ship_width / 2.0, 0.0),
    ];

    for i in player_collision.iter_mut(){
        i.x -= ship_width / 2.0;
        i.y -= ship_height / 2.0;
    }

    let mut player_ship = Player::new(&texture, player_ship_rect, Vector2::new(100.0, 100.0), PI, Some(player_collision),1);

    let mut meteor = Meteor::new(&texture, asteroid_rect, Vector2::new(100.0,100.0), 0.0, None,0);

    let mut lazer = Lazer::new(&texture, lazer_rect, Vector2::new(100.0,100.0), 0.0, None,2);

    let mut sprites:Vec<Box<dyn SpriteGeneric>> = Vec::new();

    sprites.push(Box::new(player_ship));
    sprites.push(Box::new(meteor));
    sprites.push(Box::new(lazer));

    sprites.sort_by(|a, b| (a.get_z().cmp(&b.get_z())));

    let mut event_pump = sdl_context.event_pump().unwrap();
    let mut start_time:SystemTime;
    'running: loop {
        start_time = SystemTime::now();
        canvas.clear();
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    break 'running
                },
                _ => ()
            }
        }
        let mut spawn_lazer:Option<Isometry<f32>> = None;
        //&mut Box<dyn SpriteGeneric>
        for i in sprites.iter_mut(){
            i.update();
            i.draw(&mut canvas);
            i.draw_collision(&mut canvas);
            spawn_lazer = match i.get_keyboard(event_pump.keyboard_state()) {
                return_action::spawn_lazer(x) => Some(x),
                return_action::nothing => spawn_lazer,
            }
        }

        println!("{}",sprites[0].check_collsion(sprites[1].as_ref()));

        sprites.retain(|x| !x.deleteable());

        if let Some(x) = spawn_lazer{
            let new_lazer = Lazer::new(&texture, lazer_rect, x.translation.vector, x.rotation.angle(), None,2);
            sprites.push(Box::new(new_lazer));
            sprites.sort_by(|a, b| a.get_z().cmp(&b.get_z()));
        }
        // player_ship.update();
        // meteor.draw(&mut canvas);
        // player_ship.draw(&mut canvas);
        // meteor.draw_collision(&mut canvas);
        // player_ship.draw_collision(&mut canvas);
        // let color = canvas.draw_color();
        // canvas.set_draw_color(Color::RGB(0, 0, 255));
        // let collision_test_rect = Rect::new(200,200,100,100);
        // canvas.draw_rect(collision_test_rect).unwrap();
        // let mut rect_points = Vec::new();
        // rect_points.push(convert_sdl2_point_to_parry(&collision_test_rect.top_left()));
        // rect_points.push(convert_sdl2_point_to_parry(&collision_test_rect.top_right()));
        // rect_points.push(convert_sdl2_point_to_parry(&collision_test_rect.bottom_right()));
        // rect_points.push(convert_sdl2_point_to_parry(&collision_test_rect.bottom_left()));

        // let collison_test_poly = Polyline::new(rect_points,None);
        // println!("{}",player_ship.check_collsion(&meteor));

        // canvas.set_draw_color(color);
        canvas.present();
        
        let mut frame_elapsed = start_time.elapsed().unwrap();
        let one_nano = 1000000000.0;
        let frame_rate = 60.0;
        let wanted_duration = Duration::new(0,(one_nano / frame_rate) as u32);
        if frame_elapsed > wanted_duration{
            frame_elapsed = Duration::new(0, 0);
        }
        ::std::thread::sleep(wanted_duration - frame_elapsed);
        // println!("{:?}",one_nano as u128 / start_time.elapsed().unwrap().as_nanos());
    }
}
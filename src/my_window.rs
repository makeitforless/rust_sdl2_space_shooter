use rand::{thread_rng, Rng};

use std::borrow::Borrow;
use std::f32::consts::PI;

use parry2d::math::Point as Point2;
use sdl2::libc::winsize;
use sdl2::pixels::Color;

use sdl2::Sdl;
use sdl2::pixels::PixelFormatEnum;
use sdl2::render::{Canvas, Texture, TextureCreator};
use sdl2::video::{Window, WindowContext};
use sdl2::surface::{Surface};
use sdl2::rect::{Rect, Point};

pub struct sprite<'a>{
    tex:&'a Texture<'a>,
    src_rect:Rect,
    postition:Point2<f32>,
    rotation:f32,
    delete:bool,
    collision_points:Vec<Point2<f32>>
}

pub trait sprite_generic<'a>{
    fn draw(&self,canvas:&mut Canvas<Window>);

    fn move_direct(&mut self,x:f32,y:f32);

    fn rotate(&mut self,rot:f32);

    fn move_forward(& mut self, dist:f32);

    fn update(&mut self);

    fn draw_bounding_box(&self,canvas:&mut Canvas<Window>);

    fn deleteable(&self) -> bool;
}

fn draw_sprite(s:&sprite,mut canvas:&mut Canvas<Window>){
    let (width,height) = canvas.output_size().unwrap();
        let dst_rect = Rect::new(
            (s.postition.x * width as f32) as i32 - (s.src_rect.width() as f64 /2.0) as i32,
            (s.postition.y * height as f32) as i32 - (s.src_rect.height()as f64 /2.0) as i32,
            s.src_rect.width(),
            s.src_rect.height());
        canvas.copy_ex(
            s.tex,
            s.src_rect,
            dst_rect,
            s.rotation.into(),
            None,
            false,
            false
        ).unwrap();
}

fn move_direct(s:&mut sprite,x:f32,y:f32){
    s.postition.x += x;
    s.postition.y += y;
}

fn rotate(s:&mut sprite,rot:f32){
    s.rotation += rot;
}

fn move_forward(s:&mut sprite, dist:f32){
    let rad_rot = s.rotation / 360.0 * PI * 2.0;
    let x_change = rad_rot.sin() * dist;
    let y_change = rad_rot.cos() * dist;
    s.postition.x += x_change;
    s.postition.y -= y_change;
}
fn rotated_point(point:Point,center:Point,rot:f32) -> Point{
    let (x0,y0) = center.into();
    let x0 = x0 as f32;
    let y0 = y0 as f32;
    let (x,y) = point.into();
    let x = x as f32;
    let y = y as f32;
    let x2 = x0+(x-x0)*rot.cos()+(y-y0)*rot.sin();
    let y2 = y0-(x-x0)*rot.sin()+(y-y0)*rot.cos();
    Point::new(x2 as i32,y2 as i32)
}

fn find_min_max_point(points:Vec<Point>) -> (Point,Point){
    let mut min_x = points[0].x;
    let mut max_x = points[0].x;
    let mut min_y = points[0].y;
    let mut max_y = points[0].y;
    for i in points{
        if min_x > i.x{
            min_x = i.x;
        }
        if max_x < i.x{
            max_x = i.x;
        }
        if min_y > i.y{
            min_y = i.y;
        }
        if max_y < i.y{
            max_y = i.y;
        }
    }
    (Point::new(min_x,min_y),Point::new(max_x-min_x, max_y-min_y))
}

fn calculate_bounding_points(s:&sprite,window_width:u32,window_height:u32) -> Rect{
    let real_x = ((s.postition.x * window_width as f32) - (s.src_rect.width() as f32 /2.0)) as i32;
    let real_x_2 = real_x + s.src_rect.width() as i32;
    let real_y = ((s.postition.y * window_height as f32) - (s.src_rect.height() as f32 /2.0)) as i32;
    let real_y_2 = real_y + s.src_rect.height() as i32;
    let mut top_left = Point::new(real_x,real_y);
    let mut top_right = Point::new(real_x_2,real_y);
    let mut bottom_left = Point::new(real_x,real_y_2);
    let mut bottom_right = Point::new(real_x_2,real_y_2);
    let center_x = (s.postition.x * window_width as f32);
    let center_y = (s.postition.y * window_height as f32);
    let center_point = Point::new(center_x as i32, center_y as i32);
    let rad_rot = s.rotation / 360.0 * PI * 2.0;
    top_left = rotated_point(top_left, center_point, rad_rot);
    top_right = rotated_point(top_right, center_point, rad_rot);
    bottom_left = rotated_point(bottom_left, center_point, rad_rot);
    bottom_right = rotated_point(bottom_right, center_point, rad_rot);
    let point_vec = vec![top_left,top_right,bottom_left,bottom_right];
    let (min_point, size) = find_min_max_point(point_vec);
    Rect::new(
        min_point.x,
        min_point.y,
        size.x as u32,
        size.y as u32
    )

    //get min of x , x + width transformed
    //get min of y , y + height transformed
}

fn draw_bounding_box(s:&sprite,mut canvas:&mut Canvas<Window>){
    let (width,height) = canvas.output_size().unwrap();
    let color = canvas.draw_color();
    let rad_rot = s.rotation / 360.0 * PI * 2.0; 

    // let rect_width = (s.src_rect.height() as f64 * rad_rot.sin()).abs() + (s.src_rect.width() as f64 * rad_rot.cos()).abs();
    // let rect_height = (s.src_rect.height() as f64 * rad_rot.cos()).abs() + (s.src_rect.width() as f64 * rad_rot.sin()).abs();
    // println!("curr rot is {} sin is {} cos is {} abs sin width {} abs cos width {} abs sin height {} abs cos height {}",
    // rad_rot,
    // rad_rot.sin(),
    // rad_rot.cos(),
    // (s.src_rect.width() as f64 * rad_rot.sin()).abs(),
    // (s.src_rect.width() as f64 * rad_rot.cos()).abs(),
    // (s.src_rect.height() as f64 * rad_rot.cos()).abs(),
    // (s.src_rect.height() as f64 * rad_rot.sin()).abs()
    // );
    // println!("s rot in degrees {} in rads {} width {} height {} src width {} src height {}",s.rotation,rad_rot,rect_width,rect_height,s.src_rect.width(),s.src_rect.height());
    canvas.set_draw_color(Color::RGB(255,0,0));
    // let p = Point::new(((s.postition.x * width as f64)  - (s.src_rect.width() as f64 /2.0)) as i32, s.postition.y as i32);
    // let bounding_rect = calculate_bounding_points(s, width, height);
    // canvas.draw_rect(bounding_rect).unwrap();
    // let mut points = Vec::new();
    //     let radius = bounding_rect.width() as f32 /2.0;
    //     let circle_center = Point::new((s.postition.x * width as f64) as i32, (s.postition.y * height as f64) as i32); //Point::new(bounding_rect.x,bounding_rect.y);

    //     for i in 0..=(360/36){
    //         let x = i * 36;
    //         let as_rad = x as f32 / 360.0 * PI * 2.0;
    //         points.push(Point::new(circle_center.x+(radius as f32 * as_rad.sin()) as i32,circle_center.y+(radius as f32 *as_rad.cos()) as i32));
    //     }
    //     canvas.draw_lines(&points[..]).unwrap();
    let mut bounding_points = Vec::new();
    for i in &s.collision_points{
        let s = Point::new(
            i.x as i32 + (s.postition.x * width as f32) as i32 - (s.src_rect.width() as f32 / 2.0) as i32,
            i.y as i32 + (s.postition.y * height as f32) as i32 - (s.src_rect.height() as f32 / 2.0) as i32);
        bounding_points.push(s)
    }
    bounding_points.push(bounding_points[0]);
    canvas.draw_lines(&bounding_points[..]).unwrap();
    canvas.set_draw_color(color);
}

pub struct player<'a>{
    sprite:sprite<'a>
}

impl<'a> player<'a>{
    pub fn new(tex:&'a Texture,src:Rect,dst:Point2<f32>,rot:f32,collision_points:Option<Vec<Point2<f32>>>) -> Self{
        let rotation_iso = parry2d::math::Isometry::new(parry2d::math::Vector::new(0.0,0.0),rot);
        if collision_points != None{
            for i in collision_points.as_ref().unwrap(){
                parry2d::transformation::utils::transform(&mut [*i], rotation_iso);
            }
        }
        let mut base_box:[Point2<f32>;4] = [
            Point2::new(0.0,0.0),
            Point2::new(0.0,src.height() as f32),
            Point2::new(src.width() as f32, src.height() as f32),
            Point2::new(src.width() as f32, 0.0)
        ];
        parry2d::transformation::utils::transform(&mut base_box, rotation_iso);
        player{
             sprite:sprite { 
                tex: tex,
                src_rect: src,
                postition: dst,
                rotation: rot ,
                delete:false,
                collision_points:match collision_points {
                    Some(x) => x,
                    None => 
                    base_box.into()
                }
            }
         }
     }
}

impl<'a>sprite_generic<'a> for player<'a> {
    
    fn draw(&self,mut canvas:&mut Canvas<Window>){
        draw_sprite(&self.sprite, canvas);
    }

    fn move_direct(&mut self,x:f32,y:f32) {
        move_direct(&mut self.sprite, x, y);
    }

    fn rotate(&mut self,rot:f32) {
        rotate(&mut self.sprite, rot)
    }

    fn move_forward(& mut self, dist:f32) {
        move_forward(&mut self.sprite, dist)
    }

    fn update(&mut self){
        self.rotate(1.0);
    }

    fn draw_bounding_box(&self,canvas:&mut Canvas<Window>){
        draw_bounding_box(&self.sprite, canvas);
        
    }

    fn deleteable(&self) -> bool{
        self.sprite.delete
    }
}

pub struct asteroid<'a>{
    sprite:sprite<'a>
}

impl<'a> asteroid<'a>{
    pub fn new(tex:&'a Texture,src:Rect,dst:Point2<f32>,rot:f32,collision_points:Option<Vec<Point2<f32>>>) ->Self{
        let rotation_iso = parry2d::math::Isometry::new(parry2d::math::Vector::new(0.0,0.0),rot as f32);
        if collision_points != None{
            for i in collision_points.as_ref().unwrap(){
                parry2d::transformation::utils::transform(&mut [*i], rotation_iso);
            }
        }
        let mut base_box:[Point2<f32>;4] = [
            Point2::new(0.0,0.0),
            Point2::new(0.0,src.height() as f32),
            Point2::new(src.width() as f32, src.height() as f32),
            Point2::new(src.width() as f32, 0.0)
        ];
        parry2d::transformation::utils::transform(&mut base_box, rotation_iso);
        
        asteroid{
            sprite:sprite { 
                tex: tex,
                src_rect: src,
                postition: dst,
                rotation: rot,
                delete:false,
                collision_points:match collision_points {
                    Some(x) => x,
                        None => 
                        base_box.into()
                }
            }
        }
    }
}

impl<'a>sprite_generic<'a> for asteroid<'a> {
    

    fn draw(&self,mut canvas:&mut Canvas<Window>){
        draw_sprite(&self.sprite, canvas);
    }

    fn move_direct(&mut self,x:f32,y:f32) {
        move_direct(&mut self.sprite, x, y);
    }

    fn rotate(&mut self,rot:f32) {
        rotate(&mut self.sprite, rot)
    }

    fn move_forward(& mut self, dist:f32) {
        move_forward(&mut self.sprite, dist)
    }

    fn update(&mut self){
        self.rotate(1.0);
        // self.move_forward(0.001);
    }

    fn draw_bounding_box(&self,canvas:&mut Canvas<Window>){
        draw_bounding_box(&self.sprite, canvas);
    }

    fn deleteable(&self) -> bool{
        self.sprite.delete
    }
}

pub struct lazer_ball<'a>{
    sprite:sprite<'a>
}

impl<'a> lazer_ball<'a>{
    pub fn new(tex:&'a Texture,src:Rect,dst:Point2<f32>,rot:f32,collision_points:Option<Vec<Point2<f32>>>) ->Self{
        let rotation_iso = parry2d::math::Isometry::new(parry2d::math::Vector::new(0.0,0.0),rot as f32);
        if collision_points != None{
            for i in collision_points.as_ref().unwrap(){
                parry2d::transformation::utils::transform(&mut [*i], rotation_iso);
            }
        }
        let mut base_box:[Point2<f32>;4] = [
            Point2::new(0.0,0.0),
            Point2::new(0.0,src.height() as f32),
            Point2::new(src.width() as f32, src.height() as f32),
            Point2::new(src.width() as f32, 0.0)
        ];
        parry2d::transformation::utils::transform(&mut base_box, rotation_iso);
        lazer_ball{
            sprite:sprite { 
                tex: tex,
                src_rect: src,
                postition: dst,
                rotation: rot,
                delete:false,
                collision_points:match collision_points {
                    Some(x) => x,
                    None => 
                    base_box.into()
                }
            }
        }
    }
}

impl<'a>sprite_generic<'a> for lazer_ball<'a> {
    

    fn draw(&self,mut canvas:&mut Canvas<Window>){
        draw_sprite(&self.sprite, canvas);
    }

    fn move_direct(&mut self,x:f32,y:f32) {
        move_direct(&mut self.sprite, x, y);
    }

    fn rotate(&mut self,rot:f32) {
        rotate(&mut self.sprite, rot)
    }

    fn move_forward(& mut self, dist:f32) {
        move_forward(&mut self.sprite, dist)
    }

    fn update(&mut self){
        self.rotate(5.0);
        // if self.sprite.rotation > 360.0{
        //     self.sprite.delete = true;
        // }
        // self.move_forward(0.001);
    }

    fn deleteable(&self) -> bool{
        self.sprite.delete
    }

    fn draw_bounding_box(&self,canvas:&mut Canvas<Window>){
        draw_bounding_box(&self.sprite, canvas);
    }
}

pub struct star<'a>{
    sprite:sprite<'a>,
    x_vel:f32,
    y_vel:f32
}

impl<'a> star<'a>{
    pub fn new(tex:&'a Texture,src:Rect,dst:Point2<f32>,rot:f32,collision_points:Option<Vec<Point2<f32>>>) ->Self{
        let rotation_iso = parry2d::math::Isometry::new(parry2d::math::Vector::new(0.0,0.0),rot as f32);
        if collision_points != None{
            for i in collision_points.as_ref().unwrap(){
                parry2d::transformation::utils::transform(&mut [*i], rotation_iso);
            }
        }
        let mut base_box:[Point2<f32>;4] = [
            Point2::new(0.0,0.0),
            Point2::new(0.0,src.height() as f32),
            Point2::new(src.width() as f32, src.height() as f32),
            Point2::new(src.width() as f32, 0.0)
        ];
        parry2d::transformation::utils::transform(&mut base_box, rotation_iso);
        let mut rng = rand::thread_rng();
        let x: f32 = (rng.gen::<f32>() - 0.5) * 0.01; // generates a float between 0 and 1
        let y: f32 = (rng.gen::<f32>() ) * -0.01; // generates a float between 0 and 1

        star{
            sprite:sprite { 
                tex: tex,
                src_rect: src,
                postition: dst,
                rotation: rot,
                delete:false,
                collision_points:match collision_points {
                    Some(x) => x,
                    None => 
                    base_box.into()
                }
            },
            x_vel:x,
            y_vel:y
        }
    }
}

impl<'a>sprite_generic<'a> for star<'a> {
    

    fn draw(&self,mut canvas:&mut Canvas<Window>){
        draw_sprite(&self.sprite, canvas);
    }

    fn move_direct(&mut self,x:f32,y:f32) {
        move_direct(&mut self.sprite, x, y);
    }

    fn rotate(&mut self,rot:f32) {
        rotate(&mut self.sprite, rot)
    }

    fn move_forward(& mut self, dist:f32) {
        move_forward(&mut self.sprite, dist)
    }

    fn update(&mut self){
        self.rotate(1.0);
        self.y_vel += 0.001;
        self.sprite.postition.x += self.x_vel;
        self.sprite.postition.y += self.y_vel;
        if self.sprite.postition.y > 1.0{
            self.sprite.delete = true;
        }
        // self.move_forward(0.001);
    }

    fn deleteable(&self) -> bool{
        self.sprite.delete
    }

    fn draw_bounding_box(&self,canvas:&mut Canvas<Window>){
        draw_bounding_box(&self.sprite, canvas);
    }
}